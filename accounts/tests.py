from django.contrib.auth.models import User
from django.test import TestCase

from accounts.models import Tracker, Company


class AccountsModelsTest(TestCase):
    def setUp(self):
        self.company = Company.objects.create(name="Philipp Morris")
        self.rome = User.objects.create(username="Rome", email="rome@email.com")
        self.ahmed = User.objects.create(username="Ahmed", email="ahmed@email.com")

    def test_company_has_employees(self):
        self.company.employees.set([self.rome.pk, self.ahmed.pk])
        self.assertEqual(self.company.employees.count(), 2)

    def test_tracker_has_owner_company(self):
        tracker = Tracker.objects.create(name="Philips301", owner_company=self.company, owner_person=self.rome)
        self.assertEqual(tracker.owner_company, self.company)
        self.assertEqual(tracker.owner_person, self.rome)
        self.assertEqual(tracker.owner, self.company)

    def test_tracker_has_owner_person(self):
        tracker = Tracker.objects.create(name="Philips301", owner_person=self.rome)
        self.assertEqual(tracker.owner, self.rome)

    def test_tracker_has_no_owner(self):
        tracker = Tracker.objects.create(name="Philips301")
        with self.assertRaises(AssertionError):
            print(tracker.owner)
