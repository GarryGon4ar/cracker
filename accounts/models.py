from django.contrib.auth.models import User

from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=100)
    employees = models.ManyToManyField(User)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Tracker(models.Model):
    """Test parser."""
    name = models.CharField(max_length=100)
    owner_company = models.ForeignKey(Company, null=True, blank=True,
                                      on_delete=models.CASCADE)
    owner_person = models.ForeignKey(User, null=True, blank=True,
                                     on_delete=models.CASCADE)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    @property
    def owner(self):
        if self.owner_company_id is not None:
            return self.owner_company
        if self.owner_person_id is not None:
            return self.owner_person
        raise AssertionError("Neither 'owner_group' nor 'owner_person' is set")
